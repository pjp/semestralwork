#!/bin/bash

if [ ! -d llvm-src ]; then

  mkdir -p llvm-src
  mkdir -p llvm-obj
#  sudo mount -t tmpfs none llvm-src
#  sudo mount -t tmpfs none llvm-obj

  svn checkout https://llvm.org/svn/llvm-project/llvm/trunk@229311 llvm-src
  git clone http://gitlab.fit.cvut.cz/pjp/llvm-emptyfrontend.git llvm-src/projects/sfe

fi

cd llvm-obj
../llvm-src/configure CC=gcc CXX=g++
make -j5

cd Debug+Asserts/examples
touch aaa.sfe
./Sfe aaa.sfe
